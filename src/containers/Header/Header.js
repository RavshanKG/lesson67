import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import './Header.css';

const MainMenu = () => {
  return (
    <div>
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="home">Home</a>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavLink  className="my-project" to="myProject">
            My project
          </NavLink>
        </Nav>
      </Navbar>;
    </div>
  )
}
export default MainMenu;