import React from 'react';
import './Calculator.css'
import {connect} from 'react-redux';

class Calculator extends React.Component {
  render (){
    console.log(this.props.digits)
    return (
        <div className="container">
          <h3 className="display">{this.props.digits}</h3>
          <button onClick={() => this.props.addButton(1)} >1</button>
          <button onClick={() => this.props.addButton(2)}>2</button>
          <button onClick={() => this.props.addButton(3)}>3</button>
          <button onClick={() => this.props.addButton(4)}>4</button>
          <button onClick={() => this.props.addButton(5)}>5</button>
          <button onClick={() => this.props.addButton(6)}>6</button>
          <button onClick={() => this.props.addButton(7)}>7</button>
          <button onClick={() => this.props.addButton(8)}>8</button>
          <button onClick={() => this.props.addButton(9)}>9</button>
          <button onClick={() => this.props.addButton(0)}>0</button>
          <button onClick={() => this.props.deleteDigit()}>x</button>
          <button onClick={() => this.props.correctPasswordEntered()}>e</button>
        </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    digits: state.digits
  }
};


const mapDispatchToProps = dispatch => {
  return {
    addButton: (value) => dispatch({type: 'ADD_NUMBER', value: value}),
    deleteDigit: (value) => dispatch({type: 'DELETE_NUMBER'}),
    correctPasswordEntered: (value) => dispatch({type: 'CORRECT_PASSWORD'})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);