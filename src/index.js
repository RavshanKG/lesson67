import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from "react-router-dom";

import {createStore} from 'redux';
import reducer from './store/reducer';
import {Provider} from "react-redux";

const store = createStore(reducer);

const wrapApp = (App, store) => (
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>
)

ReactDOM.render(wrapApp(App, store), document.getElementById('root'));
registerServiceWorker();
