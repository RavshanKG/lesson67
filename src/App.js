import React, { Component } from 'react';
import './App.css';
import Header from "./containers/Header/Header";
import {Route, Switch} from "react-router-dom";
import Calculator from "./containers/MyProject/Calculator";

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route path="/home" />
          <Route path="/myProject" component={Calculator}/>
        </Switch>
      </div>
    );
  }
}

export default App;
