const initialState = {
  digits: ''
}


const password = '1234';

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case 'ADD_NUMBER':
      return {digits: state.digits + action.value};
    case 'DELETE_NUMBER':
      return {digits: state.digits.slice(0, -1)};
    case 'CORRECT_PASSWORD':
      if (state.digits === password) {
        return {digits: "Correct password"};
      } else {
        return {digits: "Wrong password"}
      }
    default:
      return state;
  }
}


export default reducer;